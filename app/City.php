<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'name',
        'slug'
    ];

    public $timestamps = false;


    public function times_spans()
    {
        return $this->hasMany('App\Time_span');
    }

}
