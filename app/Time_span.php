<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time_span extends Model
{
    protected $table = 'times_spans';
    protected $fillable = [
        'span'
    ];

    public function attachToCity($city_id){
        
        $this->city_id = $city_id;

        $this->save();
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

}
