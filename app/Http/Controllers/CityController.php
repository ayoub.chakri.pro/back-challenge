<?php

namespace App\Http\Controllers;

// utilities
use DateTime;

// requests
use App\http\Requests\CreateCityRequest;
use Illuminate\Http\Request;

// models
use App\City;
use App\Time_span;
use App\Excluded_time_span;

// resources
use App\Http\Resources\DeliveryTimesResource;



class CityController extends Controller
{

    public function store(CreateCityRequest $request ){

        $city = City::create( $request->all() );
        return response()->json($city, 201);

    }


    public function delivery_dates_times($city_id, $number_of_days_to_get){
        
        // getting date
        $today = new DateTime(); 

        $result = [];

        // starting from today 
        for ($i=0; $i < $number_of_days_to_get ; $i++) { 

            // getting ids of excluded spans for a specific day
            $excluded =  Excluded_time_span::select('time_span_id')->where('date',$today->format('Y-m-d'))->get()->toArray();

            // preparing the ojbect delivery time
            $day_result = [

                'day_name'=>$today->format('l'), //getting complete name of the day
                'date'=>$today->format('Y-m-d'), //getting date format

                // query for selecting delivry time for a given city whithout the excluded ones
                'delivery_times'=> DeliveryTimesResource::collection(Time_span::join('cities', 'cities.id', '=', 'times_spans.city_id')->whereNotIn('times_spans.id',$excluded)->get())
                
            ];
            
            // pushing result day to the results
            $result[] = $day_result;
            
            // move to the next day
            date_add($today, date_interval_create_from_date_string('1 days'));

        }

        return response()->json(['dates'=>$result], 200);
        
    }


}
