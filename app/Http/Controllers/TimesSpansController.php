<?php

namespace App\Http\Controllers;

// requests
use App\http\Requests\CreateTimesSpansRequest;
use App\http\Requests\AttachDeliveryTimeRequest;
use Illuminate\Http\Request;

// models
use App\Time_span;
use App\Excluded_time_span;
use App\City;

class TimesSpansController extends Controller
{

    public function store(CreateTimesSpansRequest $request ){

        $time_span = Time_span::create( $request->all() );
        return response()->json($time_span, 201);

    }


    public function attach_delivery_times(AttachDeliveryTimeRequest $request , $city_id){

        $time_span = Time_span::find($request->time_span_id);
        $time_span->attachToCity($city_id);

        return response()->json($time_span, 200);

    }


    public function exclude($time_span_id, $date){

        return response()->json( $this->addExcludeTimeSpan($time_span_id, $date), 200);

    }


    public function exclude_date($city_id, $date){

        $city = City::find($city_id);

        // get the list of timespans and exculde one by one
        $city->times_spans->map(function ($time_span) use ($date) {
            $this->addExcludeTimeSpan($time_span->id, $date);
        });
    
    }


    // fuction allow us to add an exclude row 
    private function addExcludeTimeSpan($time_span_id, $date){

        $excluded_time_span = Excluded_time_span::where('date', '=', $date)->where('time_span_id', '=', $time_span_id);

        //testing if the record already exist
        if ( ! $excluded_time_span->exists() ) {

            return Excluded_time_span::create([
                'date' => $date, 
                'time_span_id' => $time_span_id
            ]);
        
        }else{

            return $excluded_time_span->first();

        }

    }


}
