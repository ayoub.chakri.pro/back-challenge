<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use DateTime;

class DeliveryTimesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'delivery_at'=>$this->span,
            'created_at'=> (new DateTime($this->created_at))->format('Y-m-d H:m:s'),
            'updated_at'=> (new DateTime($this->updated_at))->format('Y-m-d H:m:s')
        ];
    }
}
