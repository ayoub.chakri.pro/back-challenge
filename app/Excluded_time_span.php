<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excluded_time_span extends Model
{

    protected $table = 'excluded_times_spans';

    protected $fillable = [
        'date',
        'time_span_id'
    ];

    

}
