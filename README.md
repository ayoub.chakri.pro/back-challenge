You can now access the server at http://localhost:8000/api

## API docs

- post `/city` - add city - post parameters (name, slug)
- post `/delivery-times` - add delivery time - post parameters (span)
- post `/city/{city_id}/delivery-times` - attach delivery time to city - post parameters (time_span_id)
- post `/delivery-times/{time_span_id}/exclude/{date}` - Exclude city delivery times span from  delivery date
- post `/city/{city_id}/exclude_date/{date}` - excluding daily delivery times 
- get `/city/{city_id}/delivery-dates-times/{number_of_days_to_get}` - getting delivery time for a given city and days

## Installation steps

After cloning repo, Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate


Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve