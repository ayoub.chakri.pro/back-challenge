<?php

Route::post('city', 'CityController@store');

Route::post('delivery-times', 'TimesSpansController@store');

Route::post('city/{city_id}/delivery-times', 'TimesSpansController@attach_delivery_times');

Route::post('delivery-times/{time_span_id}/exclude/{date}', 'TimesSpansController@exclude');

Route::post('city/{city_id}/exclude_date/{date}', 'TimesSpansController@exclude_date');

Route::get('city/{city_id}/delivery-dates-times/{number_of_days_to_get}', 'CityController@delivery_dates_times');




